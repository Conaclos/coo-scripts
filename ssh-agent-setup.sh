#!/bin/sh

if test -f ~/.profile; then
    echo 'File ~/.profile already exists' >&2
    exit 1
else
    if test -f  ~/.ssh/config; then
        echo 'File ~/.ssh/config already exists' >&2
        exit 1
    else
        echo 'eval $(ssh-agent) > /dev/null' > ~/.profile &&
        echo 'AddKeysToAgent yes' > ~/.ssh/config
    fi
fi

